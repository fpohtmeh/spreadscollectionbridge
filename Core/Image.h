#ifndef IMAGE_H
#define IMAGE_H

#include <QSize>
#include <QString>

struct Image
{
    QString path;
    QSize size;
    Qt::AspectRatioMode aspectRatioMode{Qt::KeepAspectRatio};

    bool operator==(const Image &other) const
    {
        return path == other.path && size == other.size && aspectRatioMode == other.aspectRatioMode;
    }
};

#endif // IMAGE_H
