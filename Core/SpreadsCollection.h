#ifndef SPREADSCOLLECTION_H
#define SPREADSCOLLECTION_H

#include "ObservableCollection.h"
#include "Spread.h"

class SpreadsCollection : public ObservableCollection
{
    Q_DISABLE_COPY(SpreadsCollection)

public:
    enum ItemChanges {
        SizeChange,
        BackgroundColorChange,
        ImageChange,
        ModificationTimeChange,
        ZIndexChange
    };

    SpreadsCollection() = default;
    ~SpreadsCollection() override = default;

    // Spreads management

    void addSpread(std::unique_ptr<Spread> spread);
    void insertSpread(int index, std::unique_ptr<Spread> spread);
    void removeSpread(int index);
    void moveSpread(int from, int to);
    void replaceSpread(int index, std::unique_ptr<Spread> spread);

    void addSpreads(std::vector<std::unique_ptr<Spread>> &&spreads);
    void clearSpreads();

    const Spread *spreadAt(int index) const;
    int spreadsCount() const;
    bool isEmpty() const;

    // Spreads changes

    bool resizeSpread(int index, const QSize &size);
    bool setSpreadBackgroundColor(int index, const QColor &color);
    bool setSpreadImage(int index, const Image &image);
    bool setSpreadModificationDate(int index, const QDateTime &time);

    void updateZIndices();

private:
    IndicesRange allIndicesRange() const override;

    std::vector<std::unique_ptr<Spread>> mItems;
};

#endif // SPREADSCOLLECTION_H
