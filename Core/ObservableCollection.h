#ifndef OBSERVABLECOLLECTION_H
#define OBSERVABLECOLLECTION_H

#include <vector>

#include "IndicesRange.h"

class CollectionObserver;
class ObservableOperationScope;

class ObservableCollection
{
public:
    virtual ~ObservableCollection();

    void addObserver(CollectionObserver *observer);
    void removeObserver(CollectionObserver *observer);
    void clearObservers();
    const std::vector<CollectionObserver *> &observers() const;

    virtual IndicesRange allIndicesRange() const = 0;

    // Sets current scope and asserts if there are multiple active scopes
    // For internal use in ObservableOperationScope
    void setCurrentScope(const ObservableOperationScope *scope);

private:
    std::vector<CollectionObserver *> mObservers;
    const ObservableOperationScope *mCurrentScope{nullptr};
};

#endif // OBSERVABLECOLLECTION_H
