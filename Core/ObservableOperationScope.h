#ifndef OBSERVABLEOPERATIONSCOPE_H
#define OBSERVABLEOPERATIONSCOPE_H

#include <vector>

#include "IndicesRange.h"

class CollectionObserver;
class ObservableCollection;

class ObservableOperationScope
{
public:
    ObservableOperationScope(ObservableCollection *observable, const IndicesRange &range);
    virtual ~ObservableOperationScope();

protected:
    const std::vector<CollectionObserver *> &observers() const;
    const IndicesRange &indicesRange() const;
    IndicesRange allIndicesRange() const;

private:
    ObservableCollection *mObservable;
    const IndicesRange mRange;
};

class InsertScope final : public ObservableOperationScope
{
public:
    InsertScope(ObservableCollection *observable, const IndicesRange &range);
    ~InsertScope() override;

private:
    const IndicesRange mRangeAfterInsert;
};

class RemoveScope final : public ObservableOperationScope
{
public:
    RemoveScope(ObservableCollection *observable, const IndicesRange &range);
    ~RemoveScope() override;

private:
    const IndicesRange mRangeAfterRemove;
};

class MoveScope final : public ObservableOperationScope
{
public:
    MoveScope(ObservableCollection *observable, const IndicesRange &range, int destination);
    ~MoveScope() override;

private:
    const int mDestination;
    const IndicesRange mRangeAfterMove;
};

class ClearScope final : public ObservableOperationScope
{
public:
    explicit ClearScope(ObservableCollection *observable);
    ~ClearScope() override;
};

class ItemChangesScope : public ObservableOperationScope
{
public:
    ItemChangesScope(ObservableCollection *observable,
                     const IndicesRange &range,
                     const std::vector<int> &itemChanges);
    virtual ~ItemChangesScope() override;

private:
    const std::vector<int> mItemChanges;
    const IndicesRange mRangeAfterChanges;
};

class ReplaceScope final : public ItemChangesScope
{
public:
    ReplaceScope(ObservableCollection *observable, const IndicesRange &range);
};

#endif // OBSERVABLEOPERATIONSCOPE_H
