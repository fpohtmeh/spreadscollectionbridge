QT += core

include($$PWD/../Root.pri)

CORE_DIR = "$$ROOT_DIR/Core"

HEADERS += \
    $$CORE_DIR/CollectionObserver.h \
    $$CORE_DIR/Image.h \
    $$CORE_DIR/IndicesRange.h \
    $$CORE_DIR/LogAssert.h \
    $$CORE_DIR/ObservableCollection.h \
    $$CORE_DIR/ObservableOperationScope.h \
    $$CORE_DIR/Spread.h \
    $$CORE_DIR/SpreadsCollection.h

SOURCES += \
    $$CORE_DIR/CollectionObserver.cpp \
    $$CORE_DIR/IndicesRange.cpp \
    $$CORE_DIR/ObservableCollection.cpp \
    $$CORE_DIR/ObservableOperationScope.cpp \
    $$CORE_DIR/SpreadsCollection.cpp
