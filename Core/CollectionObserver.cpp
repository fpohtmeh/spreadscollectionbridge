#include "CollectionObserver.h"

#include <QtGlobal>

void CollectionObserver::preInsert(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
}

void CollectionObserver::postInsert(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
}

void CollectionObserver::preRemove(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
}

void CollectionObserver::postRemove(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
}

void CollectionObserver::preMove(const IndicesRange &indicesRange, int destination)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(destination)
}

void CollectionObserver::postMove(const IndicesRange &indicesRange, int destination)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(destination)
}

void CollectionObserver::preClear() {}

void CollectionObserver::postClear() {}

void CollectionObserver::preItemChanges(const IndicesRange &indicesRange,
                                        const std::vector<int> &itemChanges)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(itemChanges);
}

void CollectionObserver::postItemChanges(const IndicesRange &indicesRange,
                                         const std::vector<int> &itemChanges)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(itemChanges);
}
