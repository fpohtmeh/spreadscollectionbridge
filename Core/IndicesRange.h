#ifndef INDICESRANGE_H
#define INDICESRANGE_H

#include <algorithm>

class IndicesRange
{
public:
    // Constructs range by first index and size
    IndicesRange(int first, int size);
    // Constructs range for single index
    IndicesRange(int index);
    // Constructs empty range
    IndicesRange();

    int first() const;
    int last() const;
    int size() const;
    bool isEmpty() const;

    bool operator==(const IndicesRange &other) const;
    bool operator!=(const IndicesRange &other) const;

    bool containsIndex(int index) const;
    template<class Container>
    bool containsIndices(const Container &container) const
    {
        return std::all_of(std::begin(container), std::end(container), [this](const auto &index) {
            return containsIndex(index);
        });
    }
    bool containsRange(const IndicesRange &range) const;
    bool isInsertableIndex(int index) const;

    IndicesRange resizedBy(int value) const;

private:
    const int mFirst{0};
    const int mSize{0};
};

#endif // INDICESRANGE_H
