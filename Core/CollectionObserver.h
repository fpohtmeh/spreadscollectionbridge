#ifndef COLLECTIONOBSERVER_H
#define COLLECTIONOBSERVER_H

#include <vector>

#include "IndicesRange.h"

class CollectionObserver
{
public:
    virtual ~CollectionObserver() = default;

    virtual void preInsert(const IndicesRange &indicesRange);
    virtual void postInsert(const IndicesRange &indicesRange);
    virtual void preRemove(const IndicesRange &indicesRange);
    virtual void postRemove(const IndicesRange &indicesRange);
    virtual void preMove(const IndicesRange &indicesRange, int destination);
    virtual void postMove(const IndicesRange &indicesRange, int destination);
    virtual void preClear();
    virtual void postClear();
    virtual void preItemChanges(const IndicesRange &indicesRange,
                                const std::vector<int> &itemChanges);
    virtual void postItemChanges(const IndicesRange &indicesRange,
                                 const std::vector<int> &itemChanges);
};

#endif // COLLECTIONOBSERVER_H
