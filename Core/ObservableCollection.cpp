#include "ObservableCollection.h"

#include "CollectionObserver.h"
#include "LogAssert.h"

ObservableCollection::~ObservableCollection()
{
    LOG_ASSERT(mObservers.empty() && "Observers are not removed before observable destruction");
    clearObservers();
}

void ObservableCollection::addObserver(CollectionObserver *observer)
{
    LOG_ASSERT(observer != nullptr);
    mObservers.push_back(observer);
}

void ObservableCollection::removeObserver(CollectionObserver *observer)
{
    LOG_ASSERT(observer != nullptr);
    const auto itEnd = mObservers.end();
    const auto it = std::find(mObservers.begin(), itEnd, observer);
    if (it != itEnd) {
        mObservers.erase(it);
    } else {
        LOG_ASSERT(false);
    }
}

void ObservableCollection::clearObservers()
{
    mObservers.clear();
}

const std::vector<CollectionObserver *> &ObservableCollection::observers() const
{
    return mObservers;
}

void ObservableCollection::setCurrentScope(const ObservableOperationScope *scope)
{
    LOG_ASSERT(scope == nullptr || mCurrentScope == nullptr);
    mCurrentScope = scope;
}
