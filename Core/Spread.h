#ifndef SPREAD_H
#define SPREAD_H

#include <memory>

#include <QColor>
#include <QDate>

#include "Image.h"

struct Spread
{
    Spread() = default;
    ~Spread() = default;

    QSize size;
    QColor backgroundColor{Qt::white};
    Image image;
    QDateTime modificationTime{QDateTime::currentDateTime()};
    int zIndex{0};
    bool isValid{false};
};

#endif // SPREAD_H
