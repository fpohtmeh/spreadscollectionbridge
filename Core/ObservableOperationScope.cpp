#include "ObservableOperationScope.h"

#include "CollectionObserver.h"
#include "LogAssert.h"
#include "ObservableCollection.h"

ObservableOperationScope::ObservableOperationScope(ObservableCollection *observale,
                                                   const IndicesRange &range)
    : mObservable(observale), mRange(range)
{
    LOG_ASSERT(observale != nullptr);
    mObservable->setCurrentScope(this);
}

ObservableOperationScope::~ObservableOperationScope()
{
    mObservable->setCurrentScope(nullptr);
}

const std::vector<CollectionObserver *> &ObservableOperationScope::observers() const
{
    return mObservable->observers();
}

const IndicesRange &ObservableOperationScope::indicesRange() const
{
    return mRange;
}

IndicesRange ObservableOperationScope::allIndicesRange() const
{
    return mObservable->allIndicesRange();
}

InsertScope::InsertScope(ObservableCollection *observable, const IndicesRange &range)
    : ObservableOperationScope(observable, range),
      mRangeAfterInsert(allIndicesRange().resizedBy(range.size()))
{
    LOG_ASSERT(allIndicesRange().isInsertableIndex(range.first()));
    for (auto &observer : observers()) {
        observer->preInsert(indicesRange());
    }
}

InsertScope::~InsertScope()
{
    LOG_ASSERT(mRangeAfterInsert == allIndicesRange());
    for (auto &observer : observers()) {
        observer->postInsert(indicesRange());
    }
}

RemoveScope::RemoveScope(ObservableCollection *observable, const IndicesRange &range)
    : ObservableOperationScope(observable, range),
      mRangeAfterRemove(allIndicesRange().resizedBy(-range.size()))
{
    LOG_ASSERT(allIndicesRange().containsRange(range));
    for (auto &observer : observers()) {
        observer->preRemove(indicesRange());
    }
}

RemoveScope::~RemoveScope()
{
    LOG_ASSERT(mRangeAfterRemove == allIndicesRange());
    for (auto &observer : observers()) {
        observer->postRemove(indicesRange());
    }
}

MoveScope::MoveScope(ObservableCollection *observable, const IndicesRange &range, int destination)
    : ObservableOperationScope(observable, range), mDestination(destination),
      mRangeAfterMove(allIndicesRange())
{
    LOG_ASSERT(allIndicesRange().containsRange(range));
    LOG_ASSERT(allIndicesRange().isInsertableIndex(destination));
    for (auto &observer : observers()) {
        observer->preMove(indicesRange(), mDestination);
    }
}

MoveScope::~MoveScope()
{
    LOG_ASSERT(mRangeAfterMove == allIndicesRange());
    for (auto &observer : observers()) {
        observer->postMove(indicesRange(), mDestination);
    }
}

ClearScope::ClearScope(ObservableCollection *observable)
    : ObservableOperationScope(observable, observable->allIndicesRange())
{
    for (auto &observer : observers()) {
        observer->preClear();
    }
}

ClearScope::~ClearScope()
{
    LOG_ASSERT(IndicesRange(0, 0) == allIndicesRange());
    for (auto &observer : observers()) {
        observer->postClear();
    }
}

ItemChangesScope::ItemChangesScope(ObservableCollection *observable,
                                   const IndicesRange &range,
                                   const std::vector<int> &itemChanges)
    : ObservableOperationScope(observable, range), mItemChanges(itemChanges),
      mRangeAfterChanges(allIndicesRange())
{
    if (indicesRange().isEmpty()) {
        return;
    }
    LOG_ASSERT(allIndicesRange().containsRange(range));
    for (auto &observer : observers()) {
        observer->preItemChanges(indicesRange(), mItemChanges);
    }
}

ItemChangesScope::~ItemChangesScope()
{
    if (indicesRange().isEmpty()) {
        return;
    }
    LOG_ASSERT(mRangeAfterChanges == allIndicesRange());
    for (auto &observer : observers()) {
        observer->postItemChanges(indicesRange(), mItemChanges);
    }
}

ReplaceScope::ReplaceScope(ObservableCollection *observable, const IndicesRange &range)
    : ItemChangesScope(observable, range, {}) // empty itemChanges mean full change
{}
