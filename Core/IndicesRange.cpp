#include "IndicesRange.h"

#include "LogAssert.h"

IndicesRange::IndicesRange(int first, int size) : mFirst(first), mSize(size)
{
    LOG_ASSERT(mFirst >= 0 && mSize >= 0);
}

IndicesRange::IndicesRange(int index) : IndicesRange(index, 1) {}

IndicesRange::IndicesRange() : IndicesRange(0, 0) {}

int IndicesRange::first() const
{
    return mFirst;
}

int IndicesRange::last() const
{
    return mFirst + mSize - 1;
}

int IndicesRange::size() const
{
    return mSize;
}

bool IndicesRange::isEmpty() const
{
    return mSize == 0;
}

bool IndicesRange::operator==(const IndicesRange &other) const
{
    return mFirst == other.mFirst && mSize == other.mSize;
}

bool IndicesRange::operator!=(const IndicesRange &other) const
{
    return !operator==(other);
}

bool IndicesRange::containsIndex(int index) const
{
    return first() <= index && index <= last();
}

bool IndicesRange::containsRange(const IndicesRange &range) const
{
    return containsIndex(range.first()) && containsIndex(range.last());
}

bool IndicesRange::isInsertableIndex(int index) const
{
    return first() <= index && index <= last() + 1;
}

IndicesRange IndicesRange::resizedBy(int value) const
{
    return IndicesRange(mFirst, mSize + value);
}
