#ifndef LOGASSERT_H
#define LOGASSERT_H

#include <QDebug>

#define LOG_ASSERT(condition) \
    if (!(condition)) { \
        qWarning().nospace() << "LOG ASSERT: \"" #condition "\" in file " << QT_MESSAGELOG_FILE \
                             << ", line " << QT_MESSAGELOG_LINE; \
    }

#endif // LOGASSERT_H
