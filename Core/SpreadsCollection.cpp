#include "SpreadsCollection.h"

#include "LogAssert.h"
#include "ObservableOperationScope.h"

void SpreadsCollection::addSpread(std::unique_ptr<Spread> spread)
{
    LOG_ASSERT(spread.get() != nullptr);
    const InsertScope scope(this, spreadsCount());
    mItems.push_back(std::move(spread));
}

void SpreadsCollection::insertSpread(int index, std::unique_ptr<Spread> spread)
{
    LOG_ASSERT(spread.get() != nullptr);
    const InsertScope scope(this, index);
    mItems.insert(mItems.begin() + index, std::move(spread));
}

void SpreadsCollection::removeSpread(int index)
{
    const RemoveScope scope(this, index);
    mItems.erase(mItems.begin() + index);
}

void SpreadsCollection::moveSpread(int from, int to)
{
    const MoveScope scope(this, from, to);
    mItems.insert(mItems.begin() + to, std::move(mItems[from]));
    if (from < to) {
        mItems.erase(mItems.begin() + from);
    } else {
        mItems.erase(mItems.begin() + from + 1);
    }
}

void SpreadsCollection::replaceSpread(int index, std::unique_ptr<Spread> spread)
{
    LOG_ASSERT(spread.get() != nullptr);
    const ReplaceScope scope(this, index);
    mItems[index] = std::move(spread);
}

void SpreadsCollection::addSpreads(std::vector<std::unique_ptr<Spread>> &&spreads)
{
    LOG_ASSERT(std::all_of(spreads.begin(), spreads.end(), [](auto &spread) {
        return spread.get() != nullptr;
    }));
    const int addedCount = static_cast<int>(spreads.size());
    const InsertScope scope(this, IndicesRange(spreadsCount(), addedCount));
    mItems.insert(mItems.end(),
                  std::make_move_iterator(spreads.begin()),
                  std::make_move_iterator(spreads.end()));
}

void SpreadsCollection::clearSpreads()
{
    const ClearScope scope(this);
    mItems.clear();
}

const Spread *SpreadsCollection::spreadAt(int index) const
{
    LOG_ASSERT(allIndicesRange().containsIndex(index));
    return mItems[index].get();
}

int SpreadsCollection::spreadsCount() const
{
    return static_cast<int>(mItems.size());
}

bool SpreadsCollection::isEmpty() const
{
    return mItems.empty();
}

bool SpreadsCollection::resizeSpread(int index, const QSize &size)
{
    LOG_ASSERT(size.isValid());
    auto &item = mItems[index];
    if (item->size == size) {
        return false;
    }
    const ItemChangesScope scope(this, index, {SizeChange});
    item->size = size;
    return true;
}

bool SpreadsCollection::setSpreadBackgroundColor(int index, const QColor &color)
{
    auto &item = mItems[index];
    if (item->backgroundColor == color) {
        return false;
    }
    const ItemChangesScope scope(this, index, {BackgroundColorChange});
    item->backgroundColor = color;
    return true;
}

bool SpreadsCollection::setSpreadImage(int index, const Image &image)
{
    auto &item = mItems[index];
    if (item->image == image) {
        return false;
    }
    const ItemChangesScope scope(this, index, {ImageChange});
    item->image = image;
    return true;
}

bool SpreadsCollection::setSpreadModificationDate(int index, const QDateTime &time)
{
    LOG_ASSERT(time.isValid());
    auto &item = mItems[index];
    if (item->modificationTime == time) {
        return false;
    }
    const ItemChangesScope scope(this, index, {ModificationTimeChange});
    item->modificationTime = time;
    return true;
}

void SpreadsCollection::updateZIndices()
{
    const auto indicesRange = allIndicesRange();
    const ItemChangesScope scope(this, indicesRange, {ZIndexChange});
    for (auto i = indicesRange.last(); i >= indicesRange.first(); --i) {
        mItems[i]->zIndex = i;
    }
}

IndicesRange SpreadsCollection::allIndicesRange() const
{
    return IndicesRange(0, spreadsCount());
}
