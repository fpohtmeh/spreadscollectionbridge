#ifndef TESTSPREADDATA_H
#define TESTSPREADDATA_H

#include "Core/SpreadsCollection.h"
#include "UI/AbstractListModelSelection.h"
#include "UI/SpreadListModel.h"

class TestSpreadData
{
public:
    using Operation = std::function<void()>;

    TestSpreadData(QObject *parent);

    QObject *spreadsProvider();
    QObject *selectedSpreadsProvider();

    const std::vector<Operation> &operations() const;

private:
    void generateOperations();

    std::unique_ptr<Spread> createGoogleSpread() const;
    std::unique_ptr<Spread> createDuckDuckGoSpread() const;
    std::unique_ptr<Spread> createEmptySpread() const;

    SpreadsCollection mCollection;
    SpreadListModel mListModel;
    AbstractListModelSelection mListSelection;

    std::vector<Operation> mOperations;
};

#endif // TESTSPREADDATA_H
