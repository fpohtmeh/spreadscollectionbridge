#include <QtTest/QtTest>

#include "Core/SpreadsCollection.h"

class TestSpreadCollection : public QObject
{
    Q_OBJECT

private:
    using SpreadPtr = std::unique_ptr<Spread>;
    using SpreadPtrVector = std::vector<SpreadPtr>;

    class Test3SpreadsCollection : public SpreadsCollection
    {
    public:
        Test3SpreadsCollection(const TestSpreadCollection *test) : SpreadsCollection()
        {
            addSpreads(test->create3Spreads());
        }
    };

    SpreadPtr createGoogleSpread() const
    {
        Image image;
        image.path = mGoogleImagePath;
        image.size = QSize(100, 150);
        image.aspectRatioMode = Qt::IgnoreAspectRatio;

        auto spread = std::make_unique<Spread>();
        spread->backgroundColor = Qt::blue;
        spread->image = image;
        spread->size = QSize(220, 220);
        spread->isValid = true;
        return std::move(spread);
    }
    SpreadPtr createDuckDuckGoSpread() const
    {
        Image image;
        image.path = mDuckDuckGoPath;
        image.size = QSize(200, 120);

        auto spread = std::make_unique<Spread>();
        spread->backgroundColor = Qt::red;
        spread->image = image;
        spread->size = QSize(350, 150);
        spread->isValid = true;
        return std::move(spread);
    }
    SpreadPtr createEmptySpread() const { return std::make_unique<Spread>(); }
    SpreadPtrVector create3Spreads() const
    {
        SpreadPtrVector spreads;
        spreads.push_back(createGoogleSpread());
        spreads.push_back(createDuckDuckGoSpread());
        spreads.push_back(createEmptySpread());
        return spreads;
    }

    bool isGoogleSpread(const Spread *spread) const
    {
        return spread->image.path == mGoogleImagePath;
    }
    bool isDuckDuckGoSpread(const Spread *spread) const
    {
        return spread->image.path == mDuckDuckGoPath;
    }
    bool isValidSpread(const Spread *spread) const { return spread->isValid; }

    const QLatin1String mGoogleImagePath{
        "https://www.google.com/images/branding/googlelogo/1x/googlelogo_light_color_272x92dp.png"};
    const QLatin1String mDuckDuckGoPath{
        "https://duckduckgo.com/assets/icons/meta/DDG-iOS-icon_152x152.png"};

private slots:
    void testAdd3()
    {
        SpreadsCollection collection;
        collection.addSpreads(create3Spreads());
        QCOMPARE(collection.spreadsCount(), 3);
        QVERIFY(isGoogleSpread(collection.spreadAt(0)));
        QVERIFY(isDuckDuckGoSpread(collection.spreadAt(1)));
        QVERIFY(!isValidSpread(collection.spreadAt(2)));
    }

    void testInsert()
    {
        SpreadsCollection collection;
        collection.addSpread(createGoogleSpread());
        QCOMPARE(collection.spreadsCount(), 1);
        QVERIFY(isGoogleSpread(collection.spreadAt(0)));
        collection.insertSpread(0, createDuckDuckGoSpread());
        QCOMPARE(collection.spreadsCount(), 2);
        QVERIFY(isDuckDuckGoSpread(collection.spreadAt(0)));
        QVERIFY(isGoogleSpread(collection.spreadAt(1)));
    }

    void testRemove2nd()
    {
        Test3SpreadsCollection collection(this);
        collection.removeSpread(1);
        QCOMPARE(collection.spreadsCount(), 2);
    }

    void testMove()
    {
        Test3SpreadsCollection collection(this);
        collection.moveSpread(2, 0);
        QVERIFY(!isValidSpread(collection.spreadAt(0)));
        QVERIFY(isGoogleSpread(collection.spreadAt(1)));
        QVERIFY(isDuckDuckGoSpread(collection.spreadAt(2)));
        // TODO: add more cases
    }

    void testReplace()
    {
        Test3SpreadsCollection collection(this);
        QVERIFY(isDuckDuckGoSpread(collection.spreadAt(1)));
        collection.replaceSpread(1, createGoogleSpread());
        QVERIFY(isGoogleSpread(collection.spreadAt(1)));
        QCOMPARE(collection.spreadsCount(), 3);
    }

    void testClear()
    {
        Test3SpreadsCollection collection(this);
        collection.clearSpreads();
        QCOMPARE(collection.spreadsCount(), 0);
    }
};

QTEST_MAIN(TestSpreadCollection)
#include "testspreadcollection.moc"
