#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTimer>

#include "TestSpreadData.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    TestSpreadData testSpreadData(&app);
    auto operations = testSpreadData.operations();
    operations.push_back([&app]() { app.quit(); });

    int timeout = 10;
    for (auto &operation : operations) {
        QTimer::singleShot(timeout, &app, operation);
        timeout += 1000;
    }

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("spreadsProvider", testSpreadData.spreadsProvider());
    engine.rootContext()->setContextProperty("selectedSpreadsProvider",
                                             testSpreadData.selectedSpreadsProvider());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    auto exitHandler = [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl) {
            QCoreApplication::exit(-1);
        }
    };
    QObject::connect(&engine,
                     &QQmlApplicationEngine::objectCreated,
                     &app,
                     exitHandler,
                     Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
