# SpreadsCollectionBridge

## Project goal
Goal is to demonstrate how we can separate items collection and QML presenter for it

## Structure
Heart of the project is 2 classes:

- `ObservableCollection` - class that must be subclassed by items collection class
- `AbstractListModel` - class that must be subclassed by presenter class

## Key points of integration
- `enum SpreadsCollection::ItemChanges`. List of all possible change types for a spread.
- `IndicesRange ObservableCollection::allIndicesRange`. Heart of collection validations.
- `QVector<int> AbstractListModel::rolesForItemChange(int itemChange)`. Defines which roles are affected by item single change.

## Convenient features
- `IndicesRange` class. Class that keeps range of indexes and manipulates with them as with a single object.
Unlike Qt-way it doesn't mess with usage of `first`, `last`, `size() - 1`, `index >= 0 && index < size()`.
Also it simplifies validation of collection operations.
- `ObservableOperationScope` class. It's RAII wrapper that safely processes all observers for every operation. Pre-operations handlers will be called in constructor, while post-operations will be called in destructor.
Safer and simple than Qt way, also it provides a clearer way to handle any items collection.
