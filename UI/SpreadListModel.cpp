#include "SpreadListModel.h"

#include <QImage>
#include <QUrl>

#include "Core/SpreadsCollection.h"

SpreadListModel::SpreadListModel(SpreadsCollection *collection, QObject *parent)
    : AbstractListModel(parent), mCollection(collection)
{
    setCollection(collection);
}

QVector<int> SpreadListModel::rolesForItemChange(int itemChange) const
{
    switch (itemChange) {
    case SpreadsCollection::SizeChange:
        return {WidthRole, HeightRole, IsValidRole};
    case SpreadsCollection::BackgroundColorChange:
        return {BackgroundColorRole};
    case SpreadsCollection::ImageChange:
        return {ImageSourceUrlRole, ImageSourceSizeRole, ImageKeepAspectRatio, IsValidRole};
    case SpreadsCollection::ModificationTimeChange:
        return {}; // UI don't care about modification time
    case SpreadsCollection::ZIndexChange:
        return {ZRole};
    default:
        return {};
    }
}

int SpreadListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return mCollection->spreadsCount();
}

QVariant SpreadListModel::data(const QModelIndex &index, int role) const
{
    const int spreadIndex = index.row();
    const auto &spread = mCollection->spreadAt(spreadIndex);

    switch (role) {
    case BackgroundColorRole:
        return spread->isValid ? spread->backgroundColor : Qt::black;
    case WidthRole:
        return spread->isValid ? spread->size.width() : 200;
    case HeightRole:
        return spread->isValid ? spread->size.height() : 100;
    case ImageSourceUrlRole:
        return spread->isValid ? QUrl(spread->image.path) : QUrl();
    case ImageSourceSizeRole:
        return spread->image.size;
    case ImageKeepAspectRatio:
        return spread->image.aspectRatioMode == Qt::KeepAspectRatio;
    case ZRole:
        return spread->zIndex;
    case IsValidRole:
        return spread->isValid;
    default:
        return QVariant();
    }
}

bool SpreadListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    const int spreadIndex = index.row();
    const auto &spread = mCollection->spreadAt(spreadIndex);

    switch (role) {
    case BackgroundColorRole:
        return mCollection->setSpreadBackgroundColor(spreadIndex, value.value<QColor>());
    case WidthRole: {
        auto size = spread->size;
        size.setWidth(value.toInt());
        return mCollection->resizeSpread(spreadIndex, size);
    }
    case HeightRole: {
        auto size = spread->size;
        size.setHeight(value.toInt());
        return mCollection->resizeSpread(spreadIndex, size);
    }
    case ImageSourceUrlRole:
    case ImageSourceSizeRole:
        return false;
    case ImageKeepAspectRatio: {
        auto image = spread->image;
        image.aspectRatioMode = value.toBool() ? Qt::KeepAspectRatio : Qt::IgnoreAspectRatio;
        return mCollection->setSpreadImage(spreadIndex, image);
    }
    case ZRole:
    case IsValidRole:
    default:
        return false;
    }
}

QHash<int, QByteArray> SpreadListModel::roleNames() const
{
    return {{BackgroundColorRole, "backgroundColor"},
            {WidthRole, "width"},
            {HeightRole, "height"},
            {ImageSourceUrlRole, "imageSourceUrl"},
            {ImageSourceSizeRole, "imageSourceSize"},
            {ImageKeepAspectRatio, "imageKeepAspectRatio"},
            {ZRole, "z"},
            {IsValidRole, "isValid"}};
}
