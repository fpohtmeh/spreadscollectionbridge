import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

Window {
    width: 800
    height: 800
    visible: true
    title: qsTr("Spread Collection Bridge")

    RowLayout {
        anchors.fill: parent
        spacing: 0

        ListView {
            model: spreadsProvider
            spacing: 1
            Layout.fillWidth: true
            Layout.fillHeight: true

            delegate: SpreadDelegate {
                isCurrent: selectedSpreadsProvider.indices.containsIndex(index)
            }

            readonly property var slowTransition: Transition {
                NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.InOutCubic }
            }

            add: slowTransition
            remove: slowTransition
            move: slowTransition
            displaced: slowTransition
        }

        Rectangle {
            color: "gray"
            Layout.preferredWidth: 3
            Layout.fillHeight: true
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Repeater {
                anchors.fill: parent
                model: selectedSpreadsProvider.indices.isSingle
                    ? selectedSpreadsProvider
                    : 0
                delegate: SpreadDelegate {}
            }
        }
    }
}
