#ifndef ABSTRACTLISTMODELSELECTION_H
#define ABSTRACTLISTMODELSELECTION_H

#include <QSortFilterProxyModel>

#include "IndicesGroup.h"

class AbstractListModel;
class IndicesRange;

class AbstractListModelSelection : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *indices READ indices NOTIFY indicesChanged)

public:
    explicit AbstractListModelSelection(AbstractListModel *sourceModel, QObject *parent = nullptr);
    virtual ~AbstractListModelSelection() override;

    void setIndices(const QVector<int> &indices);
    void resetIndices();
    void setIndex(int index);

signals:
    void indicesChanged();

private:
    QObject *indices();
    const IndicesRange allIndicesRange() const;

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

    AbstractListModel *mSourceModel;
    IndicesGroup mIndices;
};

#endif // ABSTRACTLISTMODELSELECTION_H
