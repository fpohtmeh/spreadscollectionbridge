import QtQuick 2.15

Rectangle {
    id: root

    property bool isCurrent: false

    width: model.width
    height: model.height
    color: model.backgroundColor
    z: model.z

    border.width: isCurrent ? 5 : 0
    border.color: "orange"

    Image {
        visible: model.isValid
        height: model.imageSourceSize.height
        width: model.imageSourceSize.width
        anchors.centerIn: parent
        source: model.imageSourceUrl
        fillMode: model.imageKeepAspectRatio ? Image.PreserveAspectFit : Image.Stretch
    }

    Text {
        visible: !model.isValid
        text: "Invalid Spread"
        color: "white"
        anchors.centerIn: parent
        font.pointSize: 18
    }
}
