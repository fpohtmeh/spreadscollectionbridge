#ifndef SPREADLISTMODEL_H
#define SPREADLISTMODEL_H

#include "AbstractListModel.h"

class SpreadsCollection;

class SpreadListModel : public AbstractListModel
{
    Q_OBJECT

public:
    SpreadListModel(SpreadsCollection *collection, QObject *parent);

private:
    enum Roles {
        BackgroundColorRole = Qt::UserRole,
        WidthRole,
        HeightRole,
        ImageSourceUrlRole,
        ImageSourceSizeRole,
        ImageKeepAspectRatio,
        ZRole,
        IsValidRole
    };

    QVector<int> rolesForItemChange(int itemChange) const override;

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    QHash<int, QByteArray> roleNames() const override;

    SpreadsCollection *mCollection;
};

#endif // SPREADLISTMODEL_H
