#ifndef INDICESGROUP_H
#define INDICESGROUP_H

#include <QObject>
#include <QSet>

class IndicesGroup : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isEmpty READ isEmpty NOTIFY isEmptyChanged)
    Q_PROPERTY(bool isSingle READ isSingle NOTIFY isSingleChanged)
    Q_PROPERTY(bool isMulti READ isMulti NOTIFY isMultiChanged)

public:
    explicit IndicesGroup(QObject *parent);

    void setIndices(const QVector<int> &indices);
    const QVector<int> &indices() const;

    int size() const;
    bool isEmpty() const;
    bool isSingle() const;
    bool isMulti() const;

    Q_INVOKABLE bool containsIndex(int index) const;

signals:
    void isEmptyChanged(bool isEmpty);
    void isSingleChanged(bool isSingle);
    void isMultiChanged(bool isMulti);

private:
    QVector<int> mIndices;
    QSet<int> mSet;
};

#endif // INDICESGROUP_H
