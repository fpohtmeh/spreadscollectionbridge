QT += quick

include($$PWD/../Core/Core.pri)

UI_DIR = "$$ROOT_DIR/UI"

HEADERS += \
    $$UI_DIR/AbstractListModel.h \
    $$UI_DIR/AbstractListModelSelection.h \
    $$UI_DIR/IndicesGroup.h \
    $$UI_DIR/SpreadListModel.h

SOURCES += \
    $$UI_DIR/AbstractListModel.cpp \
    $$UI_DIR/AbstractListModelSelection.cpp \
    $$UI_DIR/IndicesGroup.cpp \
    $$UI_DIR/SpreadListModel.cpp


RESOURCES += \
    $$UI_DIR/qml.qrc

OTHER_FILES += \
    $$UI_DIR/README.md

QML_IMPORT_PATH =
QML_DESIGNER_IMPORT_PATH =
