#include "AbstractListModel.h"

#include <QSet>

#include "Core/ObservableCollection.h"

AbstractListModel::AbstractListModel(QObject *parent) : QAbstractListModel(parent) {}

AbstractListModel::~AbstractListModel()
{
    resetCollection();
}

void AbstractListModel::setCollection(ObservableCollection *collection)
{
    if (mCollection == collection) {
        return;
    }
    if (mCollection != nullptr) {
        mCollection->removeObserver(this);
    }
    mCollection = collection;
    if (mCollection != nullptr) {
        mCollection->addObserver(this);
    }
}

void AbstractListModel::resetCollection()
{
    setCollection(nullptr);
}

const ObservableCollection *AbstractListModel::collection() const
{
    return mCollection;
}

void AbstractListModel::preInsert(const IndicesRange &indicesRange)
{
    beginInsertRows(QModelIndex(), indicesRange.first(), indicesRange.last());
}

void AbstractListModel::postInsert(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
    endInsertRows();
}

void AbstractListModel::preRemove(const IndicesRange &indicesRange)
{
    beginRemoveRows(QModelIndex(), indicesRange.first(), indicesRange.last());
}

void AbstractListModel::postRemove(const IndicesRange &indicesRange)
{
    Q_UNUSED(indicesRange)
    endRemoveRows();
}

void AbstractListModel::preMove(const IndicesRange &indicesRange, int destination)
{
    const QModelIndex index;
    beginMoveRows(index, indicesRange.first(), indicesRange.last(), index, destination);
}

void AbstractListModel::postMove(const IndicesRange &indicesRange, int destination)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(destination)
    endMoveRows();
}

void AbstractListModel::preClear()
{
    beginResetModel();
}

void AbstractListModel::postClear()
{
    endResetModel();
}

void AbstractListModel::preItemChanges(const IndicesRange &indicesRange,
                                       const std::vector<int> &itemChanges)
{
    Q_UNUSED(indicesRange)
    Q_UNUSED(itemChanges)
}

void AbstractListModel::postItemChanges(const IndicesRange &indicesRange,
                                        const std::vector<int> &itemChanges)
{
    QSet<int> uniqueRoles;
    QVector<int> changedRoles;
    for (const auto &itemChange : itemChanges) {
        const auto roles = rolesForItemChange(itemChange);
        for (const auto role : roles) {
            if (uniqueRoles.contains(role)) {
                continue;
            }
            changedRoles.push_back(role);
            uniqueRoles.insert(role);
        }
    }
    if (changedRoles.empty() && !itemChanges.empty()) {
        return;
    }
    emit dataChanged(index(indicesRange.first()), index(indicesRange.last()), changedRoles);
}
