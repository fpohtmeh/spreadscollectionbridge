#include "IndicesGroup.h"

#include "Core/LogAssert.h"

IndicesGroup::IndicesGroup(QObject *parent) : QObject{parent} {}

void IndicesGroup::setIndices(const QVector<int> &indices)
{
    if (mIndices == indices) {
        return;
    }

    const int oldSize = size();
    mIndices = indices;
    mSet = QSet<int>(mIndices.begin(), mIndices.end());
    LOG_ASSERT(mIndices.size() == mSet.size());

    const int newSize = size();
    if (oldSize == newSize) {
        return;
    }

    if (oldSize == 0 || newSize == 0) {
        emit isEmptyChanged(isEmpty());
    }
    if (oldSize == 1 || newSize == 1) {
        emit isSingleChanged(isSingle());
    }
    if (oldSize > 1 != newSize > 1) {
        emit isMultiChanged(isMulti());
    }
}

const QVector<int> &IndicesGroup::indices() const
{
    return mIndices;
}

int IndicesGroup::size() const
{
    return static_cast<int>(mIndices.size());
}

bool IndicesGroup::isEmpty() const
{
    return mIndices.isEmpty();
}

bool IndicesGroup::isSingle() const
{
    return mIndices.size() == 1;
}

bool IndicesGroup::isMulti() const
{
    return mIndices.size() > 1;
}

bool IndicesGroup::containsIndex(int index) const
{
    return mSet.contains(index);
}
