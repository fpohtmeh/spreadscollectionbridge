#ifndef ABSTRACTLISTMODEL_H
#define ABSTRACTLISTMODEL_H

#include <QAbstractListModel>

#include "Core/CollectionObserver.h"

class ObservableCollection;

class AbstractListModel : public QAbstractListModel, public CollectionObserver
{
    Q_OBJECT

public:
    explicit AbstractListModel(QObject *parent);
    virtual ~AbstractListModel() override;

    void setCollection(ObservableCollection *collection);
    void resetCollection();

    const ObservableCollection *collection() const;

protected:
    virtual QVector<int> rolesForItemChange(int itemChange) const = 0;

private:
    void preInsert(const IndicesRange &indicesRange) override;
    void postInsert(const IndicesRange &indicesRange) override;
    void preRemove(const IndicesRange &indicesRange) override;
    void postRemove(const IndicesRange &indicesRange) override;
    void preMove(const IndicesRange &indicesRange, int destination) override;
    void postMove(const IndicesRange &indicesRange, int destination) override;
    void preClear() override;
    void postClear() override;
    void preItemChanges(const IndicesRange &indicesRange,
                        const std::vector<int> &itemChanges) override;
    void postItemChanges(const IndicesRange &indicesRange,
                         const std::vector<int> &itemChanges) override;

    ObservableCollection *mCollection{nullptr};
};

#endif // ABSTRACTLISTMODEL_H
