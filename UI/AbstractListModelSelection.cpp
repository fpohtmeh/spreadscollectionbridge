#include "AbstractListModelSelection.h"

#include "AbstractListModel.h"
#include "Core/LogAssert.h"
#include "Core/ObservableCollection.h"

AbstractListModelSelection::AbstractListModelSelection(AbstractListModel *sourceModel,
                                                       QObject *parent)
    : QSortFilterProxyModel(parent ? parent : sourceModel), mSourceModel(sourceModel),
      mIndices(this)
{
    LOG_ASSERT(sourceModel != nullptr);

    setSourceModel(sourceModel);

    connect(sourceModel,
            &AbstractListModel::rowsAboutToBeInserted,
            this,
            &AbstractListModelSelection::resetIndices);
    connect(sourceModel,
            &AbstractListModel::rowsAboutToBeRemoved,
            this,
            &AbstractListModelSelection::resetIndices);
    connect(sourceModel,
            &AbstractListModel::rowsAboutToBeMoved,
            this,
            &AbstractListModelSelection::resetIndices);
    connect(sourceModel,
            &AbstractListModel::modelAboutToBeReset,
            this,
            &AbstractListModelSelection::resetIndices);
}

AbstractListModelSelection::~AbstractListModelSelection()
{
    setSourceModel(nullptr);
}

void AbstractListModelSelection::setIndices(const QVector<int> &indices)
{
    if (indices == mIndices.indices()) {
        return;
    }

    LOG_ASSERT(allIndicesRange().containsIndices(indices));
    mIndices.setIndices(indices);
    emit indicesChanged();
    invalidateFilter();
}

void AbstractListModelSelection::resetIndices()
{
    setIndices({});
}

void AbstractListModelSelection::setIndex(int index)
{
    setIndices({index});
}

QObject *AbstractListModelSelection::indices()
{
    return &mIndices;
}

const IndicesRange AbstractListModelSelection::allIndicesRange() const
{
    const auto collection = mSourceModel->collection();
    return collection ? collection->allIndicesRange() : IndicesRange();
}

bool AbstractListModelSelection::filterAcceptsRow(int sourceRow,
                                                  const QModelIndex &sourceParent) const
{
    Q_UNUSED(sourceParent)
    return mIndices.containsIndex(sourceRow);
}
