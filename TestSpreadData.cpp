#include "TestSpreadData.h"

TestSpreadData::TestSpreadData(QObject *parent)
    : mCollection(), mListModel(&mCollection, parent), mListSelection(&mListModel)
{
    generateOperations();
}

QObject *TestSpreadData::spreadsProvider()
{
    return &mListModel;
}

QObject *TestSpreadData::selectedSpreadsProvider()
{
    return &mListSelection;
}

const std::vector<TestSpreadData::Operation> &TestSpreadData::operations() const
{
    return mOperations;
}

void TestSpreadData::generateOperations()
{
    mOperations.push_back([this]() {
        mCollection.addSpread(createGoogleSpread());
        mListSelection.setIndex(0);
    });
    mOperations.push_back([this]() {
        mCollection.addSpread(createDuckDuckGoSpread());
        mListSelection.setIndices({0, 1});
    });
    mOperations.push_back([this]() { mCollection.addSpread(createEmptySpread()); });
    mOperations.push_back([this]() { mCollection.setSpreadBackgroundColor(0, Qt::darkBlue); });
    mOperations.push_back([this]() { mCollection.moveSpread(1, 0); });
    mOperations.push_back([this]() { mCollection.removeSpread(2); });
    mOperations.push_back([this]() { mCollection.insertSpread(1, createGoogleSpread()); });
    mOperations.push_back([this]() {
        mCollection.replaceSpread(1, createDuckDuckGoSpread());
        mListSelection.setIndex(2);
    });
    mOperations.push_back([this]() {
        mCollection.clearSpreads();
        mCollection.updateZIndices();
    });
    mOperations.push_back([this]() {
        std::vector<std::unique_ptr<Spread>> newSpreads;
        for (int i = 0; i < 5; ++i) {
            newSpreads.push_back(createDuckDuckGoSpread());
        }
        mCollection.addSpreads(std::move(newSpreads));
        mCollection.updateZIndices();
    });
};

std::unique_ptr<Spread> TestSpreadData::createGoogleSpread() const
{
    Image image;
    image.path = "https://www.google.com/images/branding/googlelogo/1x/"
                 "googlelogo_light_color_272x92dp.png";
    image.size = QSize(100, 150);
    image.aspectRatioMode = Qt::IgnoreAspectRatio;

    auto spread = std::make_unique<Spread>();
    spread->backgroundColor = Qt::blue;
    spread->image = image;
    spread->size = QSize(220, 220);
    spread->isValid = true;
    return std::move(spread);
}

std::unique_ptr<Spread> TestSpreadData::createDuckDuckGoSpread() const
{
    Image image;
    image.path = "https://duckduckgo.com/assets/icons/meta/DDG-iOS-icon_152x152.png";
    image.size = QSize(200, 120);

    auto spread = std::make_unique<Spread>();
    spread->backgroundColor = Qt::red;
    spread->image = image;
    spread->size = QSize(350, 150);
    spread->isValid = true;
    return std::move(spread);
}

std::unique_ptr<Spread> TestSpreadData::createEmptySpread() const
{
    return std::make_unique<Spread>();
}
